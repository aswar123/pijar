
<?php
include_once("koneksi.php");
$allData = mysqli_query($connect, "SELECT * FROM produk");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <a href="form-input.php">Tambah Barang</a>
    <table>
        <tr>
            <th>Nama Barang</th>
            <th>Keterangan</th>
            <th>Harga</th>
            <th>Jumlah</th>
            <th>option</th>

            <?php
            while ($data_barang = mysqli_fetch_array($allData)) {
                echo "<tr>";
                echo "<td>".$data_barang['nama_produk']."</td>";
                echo "<td>".$data_barang['keterangan']."</td>";
                echo "<td>".$data_barang['harga']."</td>";
                echo "<td>".$data_barang['jumlah']."</td>";
                echo "<td><a href='form-update.php?name=$data_barang[nama_produk]'>Edit</a> | <a href='delete.php?name=$data_barang[nama_produk]'>delete</a></td>";

            }
            ?>
        </tr>
    </table>
</body>
</html>